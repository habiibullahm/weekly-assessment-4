## 1. Area Of Triangle

```sh
function triArea(a, b){
    return a*b/2
}
```

Examples

triArea(3, 2) ➞ 3

triArea(7, 4) ➞ 14

triArea(10, 10) ➞ 50

##  2. Convert Hours into Second

```sh
howManySeconds=(jam)=>{
    let jumlahDetik = (jam*60)*60;
    
    return jumlahDetik
}
```

Examples

howManySeconds(2) ➞ 7200

howManySeconds(10) ➞ 36000

howManySeconds(24) ➞ 86400
## GIT Question


- How the workflow for git until it's updated to repository ?
- Whats git? What's git use for?
- How to track git progress ?

answer:

-start with clone a git repository to local, and we can work on the project.after done, we add the project with the syntax ( git add 'project files' ) then commit it with massage. last we push it to git repository.

-git is a control version, manage project git repository.

-use git cloud that we remote the project. we can use gitLab/gitHub as our git cloud. so we can track our repository progress. 


